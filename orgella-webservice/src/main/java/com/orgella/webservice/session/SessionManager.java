package com.orgella.webservice.session;

import javax.servlet.http.HttpSession;

import com.orgella.webservice.response.ServiceEnum;
import com.orgella.webservice.response.ServiceResponseTO;
import com.orgella.webservice.model.User;

public class SessionManager {
    private static final String SESSION_KEY = "ORGELLA-SESS";
    private static SessionManager instance;

    public static SessionManager getInstance() {
        if (null == instance) {
            synchronized (SessionManager.class) {
                if (null == instance) {
                    instance = new SessionManager();
                }
            }
        }
        return instance;
    }

    private SessionManager() {
    }

    public boolean isSessionExist(final HttpSession httpSession) {
        return null != httpSession.getAttribute(SESSION_KEY);

    }

    public ServiceResponseTO createSession(final User user, final HttpSession httpSession) {
        httpSession.setAttribute(SESSION_KEY, user.getEmail());
        return new ServiceResponseTO(ServiceEnum.SUCCESS);
    }

    public ServiceResponseTO removeSession(final HttpSession httpSession) {
        httpSession.removeAttribute(SESSION_KEY);
        return new ServiceResponseTO(ServiceEnum.SUCCESS);
    }
}
