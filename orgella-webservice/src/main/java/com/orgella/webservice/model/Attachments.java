package com.orgella.webservice.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "attachments")
public class Attachments implements Serializable {

    private static final long serialVersionUID = -9100030001832991425L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "attachments_id")
    private Integer attachmentsId;
    @Column(name = "content", columnDefinition = "LONGTEXT")
    private String content;
    @ManyToOne
    private Auction auction;

    public Attachments() {
    }

    public Attachments(Auction auction, String content) {
        this.auction = auction;
        this.content = content;
    }

    public Integer getAttachmentsId() {
        return attachmentsId;
    }

    public void setAttachmentsId(Integer attachmentsId) {
        this.attachmentsId = attachmentsId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((attachmentsId == null) ? 0 : attachmentsId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Attachments other = (Attachments) obj;
        if (attachmentsId == null) {
            if (other.attachmentsId != null)
                return false;
        } else if (!attachmentsId.equals(other.attachmentsId))
            return false;
        return true;
    }
}
