package com.orgella.webservice.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "auction_offer")
public class AuctionOffer implements Serializable {

    private static final long serialVersionUID = 6588019163022034821L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "auction_offer_id")
    private Integer auctionOfferId;
    @Column(name = "offer_date")
    private Date offerDate;
    @Column(name = "price")
    private Float price;
    @OneToOne
    private User user;
    @ManyToOne
    private Auction auction;

    public AuctionOffer() {
    }

    public AuctionOffer(Date offerDate, Float price, User user, Auction auction) {
        this.offerDate = offerDate;
        this.price = price;
        this.user = user;
        this.auction = auction;
    }

    public Integer getAuctionOfferId() {
        return auctionOfferId;
    }

    public void setAuctionOfferId(Integer auctionOfferId) {
        this.auctionOfferId = auctionOfferId;
    }

    public Date getOfferDate() {
        return offerDate;
    }

    public void setOfferDate(Date offerDate) {
        this.offerDate = offerDate;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((auctionOfferId == null) ? 0 : auctionOfferId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AuctionOffer other = (AuctionOffer) obj;
        if (auctionOfferId == null) {
            if (other.auctionOfferId != null)
                return false;
        } else if (!auctionOfferId.equals(other.auctionOfferId))
            return false;
        return true;
    }
}
