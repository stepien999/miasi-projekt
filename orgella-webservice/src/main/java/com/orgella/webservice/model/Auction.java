package com.orgella.webservice.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "auction")
public class Auction implements Serializable {

    private static final long serialVersionUID = -1495293796078746029L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "auction_id")
    private Integer auctionId;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "start_date")
    private Date startDate;
    @Column(name = "planned_stop_date")
    private Date plannedStopDate;
    @Column(name = "actual_stop_date")
    private Date actualStopDate;
    @Column(name = "winner")
    private Integer winner;
    @Column(name = "stopped")
    private Boolean stopped;
    @Column(name = "stop_reason")
    private String stopReason;
    @Column(name = "price")
    private Float price;
    @Column(name = "is_buy_now")
    private Boolean isBuyNow;
    @ManyToOne
    private User user;
    @ManyToOne
    private Category category;

    public Auction() {
    }

    public Auction(String name, String description, Date startDate, Date plannedStopDate, Date actualStopDate, Integer winner, Boolean stopped, String stopReason, Float price, Boolean isBuyNow, User user, Category category) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.plannedStopDate = plannedStopDate;
        this.actualStopDate = actualStopDate;
        this.winner = winner;
        this.stopped = stopped;
        this.stopReason = stopReason;
        this.price = price;
        this.isBuyNow = isBuyNow;
        this.user = user;
        this.category = category;
    }

    public Integer getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Integer auctionId) {
        this.auctionId = auctionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getPlannedStopDate() {
        return plannedStopDate;
    }

    public void setPlannedStopDate(Date plannedStopDate) {
        this.plannedStopDate = plannedStopDate;
    }

    public Date getActualStopDate() {
        return actualStopDate;
    }

    public void setActualStopDate(Date actualStopDate) {
        this.actualStopDate = actualStopDate;
    }

    public Integer getWinner() {
        return winner;
    }

    public void setWinner(Integer winner) {
        this.winner = winner;
    }

    public Boolean getStopped() {
        return stopped;
    }

    public void setStopped(Boolean stopped) {
        this.stopped = stopped;
    }

    public String getStopReason() {
        return stopReason;
    }

    public void setStopReason(String stopReason) {
        this.stopReason = stopReason;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Boolean getIsBuyNow() {
        return isBuyNow;
    }

    public void setIsBuyNow(Boolean isBuyNow) {
        this.isBuyNow = isBuyNow;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((auctionId == null) ? 0 : auctionId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Auction other = (Auction) obj;
        if (auctionId == null) {
            if (other.auctionId != null)
                return false;
        } else if (!auctionId.equals(other.auctionId))
            return false;
        return true;
    }
}
