package com.orgella.webservice.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "comments")
public class Comments implements Serializable {

    private static final long serialVersionUID = 1989321211573534320L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "comments_id")
    private Integer commentsId;
    @Column(name = "comment")
    private String comment;
    @Column(name = "possitive")
    private Boolean possitive;
    @ManyToOne
    private User fromUser;
    @ManyToOne
    private User toUser;

    public Comments() {
    }

    public Comments(String comment, Boolean possitive, User fromUser, User toUser) {
        this.comment = comment;
        this.possitive = possitive;
        this.fromUser = fromUser;
        this.toUser = toUser;
    }

    public Integer getCommentsId() {
        return commentsId;
    }

    public void setCommentsId(Integer commentsId) {
        this.commentsId = commentsId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getPossitive() {
        return possitive;
    }

    public void setPossitive(Boolean possitive) {
        this.possitive = possitive;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((commentsId == null) ? 0 : commentsId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Comments other = (Comments) obj;
        if (commentsId == null) {
            if (other.commentsId != null)
                return false;
        } else if (!commentsId.equals(other.commentsId))
            return false;
        return true;
    }
}
