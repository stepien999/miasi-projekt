package com.orgella.webservice.controller;

import com.orgella.webservice.dao.AuctionDao;
import com.orgella.webservice.dao.CategoryDao;
import com.orgella.webservice.mapper.Injector;
import com.orgella.webservice.mapper.mappers.AuctionMapper;
import com.orgella.webservice.mapper.mappers.CategoryMapper;
import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.Category;
import com.orgella.webservice.response.ServiceEnum;
import com.orgella.webservice.response.ServiceResponseTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoryController {

    @RequestMapping("/getAllCategories")
    public List<Category> getAllCategories() throws Exception {
        CategoryDao categoryDao = new CategoryDao((Injector.getInstance().getMapper(CategoryMapper.class)));
        return categoryDao.getAllCategories();
    }

    @RequestMapping(value = "/rest/saveCategory", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ServiceResponseTO saveCategory(@RequestBody Category category) {
        CategoryDao categoryDao = new CategoryDao(Injector.getInstance().getMapper(CategoryMapper.class));
        try {
            if (categoryDao.saveCategory(category)) {
                return new ServiceResponseTO(ServiceEnum.SUCCESS);
            } else {
                return new ServiceResponseTO(ServiceEnum.EXISTS);
            }
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }

    @RequestMapping(value = "/rest/updateCategory", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ServiceResponseTO updateCategory(@RequestBody Category category) {
        CategoryDao categoryDao = new CategoryDao(Injector.getInstance().getMapper(CategoryMapper.class));
        try {
            if (categoryDao.updateCategory(category)) {
                return new ServiceResponseTO(ServiceEnum.SUCCESS);
            } else {
                return new ServiceResponseTO(ServiceEnum.EXISTS);
            }
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }

    @RequestMapping(value = "/rest/deleteCategory", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ServiceResponseTO deleteCategory(@RequestBody Category category) {
        CategoryDao categoryDao = new CategoryDao(Injector.getInstance().getMapper(CategoryMapper.class));
        try {
            categoryDao.deleteCategory(category);
            return new ServiceResponseTO(ServiceEnum.SUCCESS);
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }
}
