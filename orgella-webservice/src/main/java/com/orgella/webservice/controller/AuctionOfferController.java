package com.orgella.webservice.controller;

import com.orgella.webservice.dao.AuctionDao;
import com.orgella.webservice.dao.AuctionOfferDao;
import com.orgella.webservice.mapper.Injector;
import com.orgella.webservice.mapper.mappers.AuctionMapper;
import com.orgella.webservice.mapper.mappers.AuctionOfferMapper;
import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.AuctionOffer;
import com.orgella.webservice.response.ServiceEnum;
import com.orgella.webservice.response.ServiceResponseTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AuctionOfferController {

    @RequestMapping("/rest/getAuctionOfferById")
    public AuctionOffer getAuctionOfferById(@RequestParam(value = "auctionOfferId") Integer auctionOfferId) throws Exception {
        AuctionOfferDao auctionOfferDao = new AuctionOfferDao(Injector.getInstance().getMapper(AuctionOfferMapper.class));
        return auctionOfferDao.getAuctionOfferById(auctionOfferId);
    }

    @RequestMapping(value = "/getAuctionOffersForAuction", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public List<AuctionOffer> getAuctionOffersForAuction(@RequestBody Auction auction) throws Exception {
        AuctionOfferDao auctionOfferDao = new AuctionOfferDao((Injector.getInstance().getMapper(AuctionOfferMapper.class)));
        return auctionOfferDao.getAuctionOffersForAuction(auction);
    }

    @RequestMapping(value = "/rest/saveAuctionOffer", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ServiceResponseTO saveAuctionOffer(@RequestBody AuctionOffer auctionOffer) {
        AuctionOfferDao auctionOfferDao = new AuctionOfferDao(Injector.getInstance().getMapper(AuctionOfferMapper.class));
        AuctionDao auctionDao = new AuctionDao(Injector.getInstance().getMapper(AuctionMapper.class));
        try {
            auctionOfferDao.saveAuctionOffer(auctionOffer);
            Auction auction = auctionDao.getAuctionById(auctionOffer.getAuction().getAuctionId());
            auction.setPrice(auctionOffer.getPrice());
            auctionDao.updateAuction(auction);
            return new ServiceResponseTO(ServiceEnum.SUCCESS);
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }
}
