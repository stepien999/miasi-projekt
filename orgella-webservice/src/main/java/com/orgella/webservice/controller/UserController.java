package com.orgella.webservice.controller;

import com.orgella.webservice.dao.UserDao;
import com.orgella.webservice.mapper.Injector;
import com.orgella.webservice.mapper.mappers.UserMapper;
import com.orgella.webservice.model.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @RequestMapping(value = "/hello")
    public String printHello() throws Exception {
        return "Czesc Karakany";
    }

    @RequestMapping(value = "/rest/getUserByEmail", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public User getUserByEmail(@RequestBody User user) throws Exception {
        UserDao userDao = new UserDao(Injector.getInstance().getMapper(UserMapper.class));
        return userDao.getUserByEmail(user.getEmail());
    }
}
