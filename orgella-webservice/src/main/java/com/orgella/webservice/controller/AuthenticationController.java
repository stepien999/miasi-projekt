package com.orgella.webservice.controller;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.orgella.webservice.response.ServiceEnum;
import com.orgella.webservice.response.ServiceResponseTO;
import com.orgella.webservice.service.AuthenticationService;
import com.orgella.webservice.session.SessionManager;
import com.orgella.webservice.model.User;

@RestController
public class AuthenticationController {
    private final static SessionManager SESSION = SessionManager.getInstance();

    @RequestMapping(value = "/signIn", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody ServiceResponseTO signIn(@RequestBody User user, HttpSession httpSession) {
        try {
            if (AuthenticationService.logIn(user)) {
                return SESSION.createSession(user, httpSession);
            } else {
                return new ServiceResponseTO(ServiceEnum.BAD_CREDENTIALS);
            }
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }

    @RequestMapping(value = "/logOut", produces = "application/json")
    public @ResponseBody ServiceResponseTO logout(HttpSession httpSession) throws Exception {
        AuthenticationService.logout();
        return SESSION.removeSession(httpSession);
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody ServiceResponseTO saveUser(@RequestBody User user) throws Exception {
        return AuthenticationService.signUp(user);
    }
}
