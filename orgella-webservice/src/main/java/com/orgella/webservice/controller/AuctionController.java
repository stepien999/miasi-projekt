package com.orgella.webservice.controller;

import com.orgella.webservice.dao.AuctionDao;
import com.orgella.webservice.mapper.Injector;
import com.orgella.webservice.mapper.mappers.AuctionMapper;
import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.Category;
import com.orgella.webservice.model.User;
import com.orgella.webservice.response.ServiceEnum;
import com.orgella.webservice.response.ServiceResponseTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AuctionController {

    @RequestMapping("/getAllAuctions")
    public List<Auction> getAllAuctions() throws Exception {
        AuctionDao auctionDao = new AuctionDao((Injector.getInstance().getMapper(AuctionMapper.class)));
        return auctionDao.getAllAuctions();
    }

    @RequestMapping("/getAuctionById")
    public Auction getAuctionById(@RequestParam(value = "auctionId") Integer auctionId) throws Exception {
        AuctionDao auctionDao = new AuctionDao(Injector.getInstance().getMapper(AuctionMapper.class));
        return auctionDao.getAuctionById(auctionId);
    }

    @RequestMapping("/searchAuctions")
    public List<Auction> searchAuctions(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "isBuyNow", required = false) Boolean isBuyNow, @RequestParam(value = "fromPrice", required = false) Float fromPrice, @RequestParam(value = "toPrice", required = false) Float toPrice) throws Exception {
        AuctionDao auctionDao = new AuctionDao(Injector.getInstance().getMapper(AuctionMapper.class));
        return auctionDao.searchAuctions(name, isBuyNow, fromPrice, toPrice);
    }

    @RequestMapping(value = "/getAuctionsByCategory", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public List<Auction> getAuctionsByCategory(@RequestBody Category category) throws Exception {
        AuctionDao auctionDao = new AuctionDao(Injector.getInstance().getMapper(AuctionMapper.class));
        return auctionDao.getAuctionsByCategory(category);
    }

    @RequestMapping(value = "/getAuctionsByUser", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public List<Auction> getAuctionsByUser(@RequestBody User user) throws Exception {
        AuctionDao auctionDao = new AuctionDao(Injector.getInstance().getMapper(AuctionMapper.class));
        return auctionDao.getAuctionsByUser(user);
    }

    @RequestMapping(value = "/rest/saveAuction", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Auction saveAuction(@RequestBody Auction auction) {
        AuctionDao auctionDao = new AuctionDao(Injector.getInstance().getMapper(AuctionMapper.class));
        try {
            auctionDao.saveAuction(auction);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return auction;
    }

    @RequestMapping(value = "/rest/updateAuction", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ServiceResponseTO updateAuction(@RequestBody Auction auction) {
        AuctionDao auctionDao = new AuctionDao(Injector.getInstance().getMapper(AuctionMapper.class));
        try {
            auctionDao.updateAuction(auction);
            return new ServiceResponseTO(ServiceEnum.SUCCESS);
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }

    @RequestMapping(value = "/rest/deleteAuction", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ServiceResponseTO deleteAuction(@RequestBody Auction auction) {
        AuctionDao auctionDao = new AuctionDao(Injector.getInstance().getMapper(AuctionMapper.class));
        try {
            auctionDao.deleteAuction(auction);
            return new ServiceResponseTO(ServiceEnum.SUCCESS);
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }
}
