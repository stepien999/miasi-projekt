package com.orgella.webservice.controller;

import com.orgella.webservice.dao.AuctionOfferDao;
import com.orgella.webservice.dao.CommentsDao;
import com.orgella.webservice.mapper.Injector;
import com.orgella.webservice.mapper.mappers.AuctionOfferMapper;
import com.orgella.webservice.mapper.mappers.CommentsMapper;
import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.AuctionOffer;
import com.orgella.webservice.model.Comments;
import com.orgella.webservice.model.User;
import com.orgella.webservice.response.ServiceEnum;
import com.orgella.webservice.response.ServiceResponseTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CommentsController {

    @RequestMapping("/getCommentById")
    public Comments getCommentById(@RequestParam(value = "commentsId") Integer commentsId) throws Exception {
        CommentsDao commentsDao = new CommentsDao(Injector.getInstance().getMapper(CommentsMapper.class));
        return commentsDao.getCommentById(commentsId);
    }

    @RequestMapping(value = "/getCommentsFromUser", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public List<Comments> getCommentsFromUser(@RequestBody User user) throws Exception {
        CommentsDao commentsDao = new CommentsDao((Injector.getInstance().getMapper(CommentsMapper.class)));
        return commentsDao.getCommentsFromUser(user);
    }

    @RequestMapping(value = "/getCommentsToUser", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public List<Comments> getCommentsToUser(@RequestBody User user) throws Exception {
        CommentsDao commentsDao = new CommentsDao((Injector.getInstance().getMapper(CommentsMapper.class)));
        return commentsDao.getCommentsToUser(user);
    }

    @RequestMapping(value = "/rest/saveComment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ServiceResponseTO saveComment(@RequestBody Comments comments) {
        CommentsDao commentsDao = new CommentsDao((Injector.getInstance().getMapper(CommentsMapper.class)));
        try {
            commentsDao.saveComment(comments);
            return new ServiceResponseTO(ServiceEnum.SUCCESS);
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }

    @RequestMapping(value = "/rest/updateComment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ServiceResponseTO updateComment(@RequestBody Comments comments) {
        CommentsDao commentsDao = new CommentsDao((Injector.getInstance().getMapper(CommentsMapper.class)));
        try {
            commentsDao.updateComment(comments);
            return new ServiceResponseTO(ServiceEnum.SUCCESS);
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }

    @RequestMapping(value = "/rest/deleteComment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ServiceResponseTO deleteComment(@RequestBody Comments comments) {
        CommentsDao commentsDao = new CommentsDao((Injector.getInstance().getMapper(CommentsMapper.class)));
        try {
            commentsDao.deleteComment(comments);
            return new ServiceResponseTO(ServiceEnum.SUCCESS);
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }
}
