package com.orgella.webservice.controller;

import com.orgella.webservice.dao.AttachmentsDao;
import com.orgella.webservice.dao.CommentsDao;
import com.orgella.webservice.mapper.Injector;
import com.orgella.webservice.mapper.mappers.AttachmentsMapper;
import com.orgella.webservice.mapper.mappers.CommentsMapper;
import com.orgella.webservice.model.Attachments;
import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.Comments;
import com.orgella.webservice.model.User;
import com.orgella.webservice.response.ServiceEnum;
import com.orgella.webservice.response.ServiceResponseTO;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AttachmentsController {

    @RequestMapping("/getAttachmentById")
    public Attachments getAttachmentById(@RequestParam(value = "attachmentsId") Integer attachmentsId) throws Exception {
        AttachmentsDao attachmentsDao = new AttachmentsDao(Injector.getInstance().getMapper(AttachmentsMapper.class));
        return attachmentsDao.getAttachmentById(attachmentsId);
    }

    @RequestMapping(value = "/getAttachmentsForAuction", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public List<Attachments> getAttachmentsForAuction(@RequestBody Auction auction) throws Exception {
        AttachmentsDao attachmentsDao = new AttachmentsDao(Injector.getInstance().getMapper(AttachmentsMapper.class));
        return attachmentsDao.getAttachmentsForAuction(auction);
    }

    @RequestMapping(value = "/getAttachmentForAuction", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Attachments getAttachmentForAuction(@RequestBody Auction auction) throws Exception {
        AttachmentsDao attachmentsDao = new AttachmentsDao(Injector.getInstance().getMapper(AttachmentsMapper.class));
        List attachments = new ArrayList();
        attachments = attachmentsDao.getAttachmentsForAuction(auction);
        if (!attachments.isEmpty())
            return attachmentsDao.getAttachmentsForAuction(auction).get(0);
        else return new Attachments();
    }

    @RequestMapping(value = "/rest/saveAttachment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ServiceResponseTO saveAttachment(@RequestBody Attachments attachments) {
        AttachmentsDao attachmentsDao = new AttachmentsDao(Injector.getInstance().getMapper(AttachmentsMapper.class));
        try {
            attachmentsDao.saveAttachment(attachments);
            return new ServiceResponseTO(ServiceEnum.SUCCESS);
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }

    @RequestMapping(value = "/rest/deleteAttachment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ServiceResponseTO deleteAttachment(@RequestBody Attachments attachments) {
        AttachmentsDao attachmentsDao = new AttachmentsDao(Injector.getInstance().getMapper(AttachmentsMapper.class));
        try {
            attachmentsDao.deleteAttachment(attachments);
            return new ServiceResponseTO(ServiceEnum.SUCCESS);
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }
}
