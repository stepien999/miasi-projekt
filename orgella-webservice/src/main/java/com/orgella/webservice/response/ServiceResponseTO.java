package com.orgella.webservice.response;

public class ServiceResponseTO {

    private String message = "";
    private String exception = "";

    public ServiceResponseTO(ServiceEnum message) {
        this.message = message.toString();
    }

    public ServiceResponseTO(ServiceEnum message, Exception e) {
        this.message = message.toString();
        this.setException(e.getMessage());
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}
