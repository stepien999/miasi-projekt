package com.orgella.webservice.response;

public enum ServiceEnum {
    SUCCESS, EXISTS, EXCEPTION, MAIL_EXCEPTION, BAD_CREDENTIALS
}
