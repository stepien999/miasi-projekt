package com.orgella.webservice.dao;

import com.orgella.webservice.mapper.mappers.AuctionMapper;
import com.orgella.webservice.mapper.mappers.CategoryMapper;
import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.Category;
import com.orgella.webservice.model.User;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

public class CategoryDao implements Serializable{

    private static final long serialVersionUID = -2575579990595266552L;

    private CategoryMapper categoryMapper;

    public CategoryDao(CategoryMapper categoryMapper) {
        this.categoryMapper = categoryMapper;
    }

    public List<Category> getAllCategories() throws Exception {
        return categoryMapper.getAllCategories();
    }

    public Category getCategoryById(@Param("categoryId") Integer categoryId) throws Exception {
        return categoryMapper.getCategoryById(categoryId);
    }

    public Category getCategoryByName(@Param("name") String name) throws Exception {
        return categoryMapper.getCategoryByName(name);
    }

    public boolean saveCategory(@Param("category") Category category) throws Exception {
        if (categoryMapper.countCategory(category) > 0) {
            return false;
        } else {
            categoryMapper.saveCategory(category);
            return true;
        }
    }

    public boolean updateCategory(@Param("editCategory") Category editCategory) throws Exception {
        if (categoryMapper.countCategory(editCategory) > 0) {
            return false;
        } else {
            categoryMapper.updateCategory(editCategory);
            return true;
        }
    }

    public void deleteCategory(@Param("category") Category category) throws Exception {
        categoryMapper.deleteCategory(category);
    }
}
