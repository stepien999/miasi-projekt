package com.orgella.webservice.dao;

import com.orgella.webservice.mapper.mappers.AuctionOfferMapper;
import com.orgella.webservice.mapper.mappers.CommentsMapper;
import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.AuctionOffer;
import com.orgella.webservice.model.Comments;
import com.orgella.webservice.model.User;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

public class CommentsDao implements Serializable{

    private static final long serialVersionUID = 3537871311995344904L;

    private CommentsMapper commentsMapper;

    public CommentsDao(CommentsMapper commentsMapper) {
        this.commentsMapper = commentsMapper;
    }

    public Comments getCommentById(@Param("commentsId") Integer commentsId) throws Exception {
        return commentsMapper.getCommentById(commentsId);
    }

    public List<Comments> getCommentsFromUser(@Param("user") User user) throws Exception {
        return commentsMapper.getCommentsFromUser(user);
    }

    public List<Comments> getCommentsToUser(@Param("user") User user) throws Exception {
        return commentsMapper.getCommentsToUser(user);
    }

    public void saveComment(@Param("comments") Comments comments) throws Exception {
        commentsMapper.saveComment(comments);
    }

    public void updateComment(@Param("editComments") Comments editComments) throws Exception {
        commentsMapper.updateComment(editComments);
    }

    public void deleteComment(@Param("comments") Comments comments) throws Exception {
        commentsMapper.deleteComment(comments);
    }

}
