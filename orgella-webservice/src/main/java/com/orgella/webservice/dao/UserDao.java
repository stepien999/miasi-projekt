package com.orgella.webservice.dao;

import org.apache.ibatis.annotations.Param;

import com.orgella.webservice.mapper.mappers.UserMapper;
import com.orgella.webservice.model.User;

import java.io.Serializable;
import java.util.List;

public class UserDao implements Serializable{

    private static final long serialVersionUID = 6369584637590094432L;

    private UserMapper userMapper;

    public UserDao(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public User getUserById(@Param("userId") Integer userId) throws Exception {
        return userMapper.getUserById(userId);
    }

    public User getUserByEmail(@Param("email") String email) throws Exception {
        return userMapper.getUserByEmail(email);
    }

    public boolean saveUser(@Param("user") User user) throws Exception {
        if (userMapper.countUser(user) > 0) {
            return false;
        } else {
            userMapper.saveUser(user);
            return true;
        }
    }

    public void changeUserPassword(@Param("user") User user, @Param("password") String password) throws Exception {
        userMapper.changeUserPassword(user, password);
    }

    public List<User> getAllUsers() throws Exception {
        return userMapper.getAllUsers();
    }

    public void updateUser(@Param("editUser") User editUser) throws Exception {
        userMapper.updateUser(editUser);
    }

    public void deleteUser(@Param("user") User user) throws Exception {
        userMapper.deleteUser(user);
    }
}
