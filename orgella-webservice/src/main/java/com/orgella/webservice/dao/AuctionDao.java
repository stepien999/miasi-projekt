package com.orgella.webservice.dao;

import com.orgella.webservice.mapper.mappers.AuctionMapper;
import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.Category;
import com.orgella.webservice.model.User;
import org.apache.ibatis.annotations.Param;

import javax.jws.soap.SOAPBinding;
import java.io.Serializable;
import java.util.List;

public class AuctionDao implements Serializable{

    private static final long serialVersionUID = -2575579990595266552L;

    private AuctionMapper auctionMapper;

    public AuctionDao(AuctionMapper auctionMapper) {
        this.auctionMapper = auctionMapper;
    }

    public Auction getAuctionById(@Param("auctionId") Integer auctionId) throws Exception {
        return auctionMapper.getAuctionById(auctionId);
    }

    public List<Auction> getAuctionsByCategory(@Param("category") Category category) throws Exception {
        return auctionMapper.getAuctionsByCategory(category);
    }

    public List<Auction> getAuctionsByUser(@Param("user") User user) throws Exception {
        return auctionMapper.getAuctionsByUser(user);
    }

    public List<Auction> searchAuctions(@Param("name") String name, @Param("isBuyNow") Boolean isBuyNow, @Param("fromPrice") Float fromPrice, @Param("toPrice") Float toPrice) throws Exception {
        return auctionMapper.searchAuctions("%"+name+"%", isBuyNow, fromPrice, toPrice);
    }

    public List<Auction> getAllAuctions() throws Exception {
        return auctionMapper.getAllAuctions();
    }

    public Auction saveAuction(@Param("auction") Auction auction) throws Exception {
        auctionMapper.saveAuction(auction);
        return auction;
    }

    public void updateAuction(@Param("editAuction") Auction editAuction) throws Exception {
        auctionMapper.updateAuction(editAuction);
    }

    public void deleteAuction(@Param("auction") Auction auction) throws Exception {
        auctionMapper.deleteAuction(auction);
    }
}
