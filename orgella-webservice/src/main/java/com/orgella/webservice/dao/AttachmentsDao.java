package com.orgella.webservice.dao;

import com.orgella.webservice.mapper.mappers.AttachmentsMapper;
import com.orgella.webservice.model.Attachments;
import com.orgella.webservice.model.Auction;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

public class AttachmentsDao implements Serializable{

    private static final long serialVersionUID = -8524568112077385023L;

    private AttachmentsMapper attachmentsMapper;

    public AttachmentsDao(AttachmentsMapper attachmentsMapper) {
        this.attachmentsMapper = attachmentsMapper;
    }

    public Attachments getAttachmentById(@Param("attachmentsId") Integer attachmentsId) throws Exception {
        return attachmentsMapper.getAttachmentById(attachmentsId);
    }

    public List<Attachments> getAttachmentsForAuction(@Param("auction") Auction auction) throws Exception {
        return  attachmentsMapper.getAttachmentsForAuction(auction);
    }

    public void saveAttachment(@Param("attachments") Attachments attachments) throws Exception {
        attachmentsMapper.saveAttachment(attachments);
    }

    public void deleteAttachment(@Param("attachments") Attachments attachments) throws Exception {
        attachmentsMapper.deleteAttachment(attachments);
    }

}
