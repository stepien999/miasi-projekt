package com.orgella.webservice.dao;

import com.orgella.webservice.mapper.mappers.AuctionMapper;
import com.orgella.webservice.mapper.mappers.AuctionOfferMapper;
import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.AuctionOffer;
import com.orgella.webservice.model.Category;
import com.orgella.webservice.model.User;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

public class AuctionOfferDao implements Serializable{

    private static final long serialVersionUID = -1018239065484630819L;

    private AuctionOfferMapper auctionOfferMapper;

    public AuctionOfferDao(AuctionOfferMapper auctionOfferMapper) {
        this.auctionOfferMapper = auctionOfferMapper;
    }

    public AuctionOffer getAuctionOfferById(@Param("auctionOfferId") Integer auctionOfferId) throws Exception {
        return auctionOfferMapper.getAuctionOfferById(auctionOfferId);
    }

    public List<AuctionOffer> getAuctionOffersForAuction(@Param("auction") Auction auction) throws Exception {
        return auctionOfferMapper.getAuctionOffersForAuction(auction);
    }

    public void saveAuctionOffer(@Param("auctionOffer") AuctionOffer auctionOffer) throws Exception {
        if (auctionOfferMapper.countAuctionOffer(auctionOffer) > 0) {
            /*AuctionOffer newAuctionOffer = auctionOfferMapper.getAuctionOfferByUserAndAuction(auctionOffer);
            auctionOffer.setAuctionOfferId(newAuctionOffer.getAuctionOfferId());*/
            auctionOfferMapper.updateAuctionOffer(auctionOffer);
        } else {
            auctionOfferMapper.saveAuctionOffer(auctionOffer);
        }
    }

    public void deleteAuctionOffer(@Param("auctionOffer") AuctionOffer auctionOffer) throws Exception {
        auctionOfferMapper.deleteAuctionOffer(auctionOffer);
    }

}
