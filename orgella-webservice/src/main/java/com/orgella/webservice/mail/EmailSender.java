package com.orgella.webservice.mail;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.orgella.webservice.model.User;

public class EmailSender {
    private final static String USERNAME = "orgella2016@yahoo.com";
    private final static String PASSWORD = "q1w2e3r4";
    private final static String HOST = "smtp.mail.yahoo.com";
    private final static String PROTOCOL = "smtps";
    private static final String PORT = "465";
    private static final String AUTH = "true";

    private static Properties getProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.host", HOST);
        props.put("mail.smtp.user", USERNAME);
        props.put("mail.smtp.password", PASSWORD);
        props.put("mail.smtp.port", PORT);
        props.put("mail.smtp.auth", AUTH);
        props.put("mail.debug", "true");
        return props;
    }

    private static Session getSession() {
        return Session.getInstance(getProperties(), new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USERNAME, PASSWORD);
            }
        });
    }

    private static Message getMessage(User user, EmailMessage emailMessage, Session session)
            throws AddressException, MessagingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(USERNAME));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user.getEmail()));
        message.setSubject(EmailMessage.getSubject());
        message.setText(emailMessage.getText());
        return message;
    }

    private static void sendMessage(Message message, Transport transport) throws MessagingException {
        transport.connect(HOST, USERNAME, PASSWORD);
        transport.sendMessage(message, message.getAllRecipients());
    }

    public static void sendMail(User user, String userPassword) throws AddressException, MessagingException {
        EmailMessage emailMessage = new EmailMessage(user, userPassword);
        Session session = getSession();
        Message message = getMessage(user, emailMessage, session);
        Transport transport = session.getTransport(PROTOCOL);
        sendMessage(message, transport);
    }
}
