package com.orgella.webservice.mail;

import com.orgella.webservice.model.User;

public class EmailMessage {

    private final static String SUBJECT = "Twoje hasło do aplikacji Orgella";
    private String password;
    private User user;

    public EmailMessage(User user, String password) {
        this.setUser(user);
        this.setPassword(password);
    }

    public String getText() {
        if(user.getFirstName() == null) return "Twoje hasło to: " + password;
        else return user.getFirstName() + ", twoje hasło to: " + password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static String getSubject() {
        return SUBJECT;
    }
}
