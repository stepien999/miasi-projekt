package com.orgella.webservice.mapper.mappers;

import com.orgella.webservice.model.Attachments;
import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.Comments;
import com.orgella.webservice.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AttachmentsMapper {

    Attachments getAttachmentById(@Param("attachmentsId") Integer attachmentsId) throws Exception;

    List<Attachments> getAttachmentsForAuction(@Param("auction") Auction auction) throws Exception;

    void saveAttachment(@Param("attachments") Attachments attachments) throws Exception;

    void deleteAttachment(@Param("attachments") Attachments attachments) throws Exception;

}
