package com.orgella.webservice.mapper;

import java.io.IOException;
import java.io.InputStream;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class Injector {

    private SqlSessionFactory sqlSessionFactory;
    private final static String RESOURCES = "mybatis-config.xml";
    private SqlSession session;
    private static boolean isSessionClosed = false;

    private static volatile Injector instance = null;

    public static Injector getInstance() {
        if (instance == null || isSessionClosed) {
            synchronized (Injector.class) {
                if (instance == null || isSessionClosed) {
                    instance = new Injector();
                }
            }
        }
        return instance;
    }

    private Injector() {
        buildSession();
    }

    private void buildSession() {
        try {
            InputStream inputStream = Resources.getResourceAsStream(RESOURCES);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
            session = sqlSessionFactory.openSession(true);
        } catch (IOException e) {
        }
    }

    public <T> T getMapper(Class<T> mapperClass) {
        try {
            return session.getMapper(mapperClass);
        } catch (Exception e) {
        }
        return null;
    }

    public void commitSession() {
        session.commit();
    }

    public void closeSession() {
        isSessionClosed = true;
        session.close();
    }

}