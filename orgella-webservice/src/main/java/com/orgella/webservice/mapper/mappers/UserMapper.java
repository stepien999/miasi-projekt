package com.orgella.webservice.mapper.mappers;

import org.apache.ibatis.annotations.Param;
import com.orgella.webservice.model.User;

import java.util.List;

public interface UserMapper {

    User getUserById(@Param("userId") Integer userId) throws Exception;

    User getUserByEmail(@Param("email") String email) throws Exception;

    List<User> getAllUsers() throws Exception;

    void changeUserPassword(@Param("user") User user, @Param("password") String password) throws Exception;

    boolean saveUser(@Param("user") User user) throws Exception;

    Integer countUser(@Param("user") User user) throws Exception;

    void updateUser(@Param("editUser") User editUser) throws Exception;

    void deleteUser(@Param("user") User user) throws Exception;

}
