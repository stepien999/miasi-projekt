package com.orgella.webservice.mapper.mappers;

import com.orgella.webservice.model.Comments;
import com.orgella.webservice.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentsMapper {

    Comments getCommentById(@Param("commentsId") Integer commentsId) throws Exception;

    List<Comments> getCommentsFromUser(@Param("user") User user) throws Exception;

    List<Comments> getCommentsToUser(@Param("user") User user) throws Exception;

    void saveComment(@Param("comments") Comments comments) throws Exception;

    void updateComment(@Param("editComments") Comments editComments) throws Exception;

    void deleteComment(@Param("comments") Comments comments) throws Exception;

}
