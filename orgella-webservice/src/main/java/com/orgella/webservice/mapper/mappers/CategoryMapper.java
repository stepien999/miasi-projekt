package com.orgella.webservice.mapper.mappers;

import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.Category;
import com.orgella.webservice.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategoryMapper {

    Category getCategoryById(@Param("categoryId") Integer categoryId) throws Exception;

    Category getCategoryByName(@Param("name") String name) throws Exception;

    List<Category> getAllCategories() throws Exception;

    void saveCategory(@Param("category") Category category) throws Exception;

    void updateCategory(@Param("editCategory") Category editCategory) throws Exception;

    void deleteCategory(@Param("category") Category category) throws Exception;

    Integer countCategory(@Param("category") Category category) throws Exception;
}
