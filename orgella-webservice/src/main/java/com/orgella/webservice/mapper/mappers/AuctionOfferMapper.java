package com.orgella.webservice.mapper.mappers;

import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.AuctionOffer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AuctionOfferMapper {

    AuctionOffer getAuctionOfferById(@Param("auctionOfferId") Integer auctionOfferId) throws Exception;

    AuctionOffer getAuctionOfferByUserAndAuction(@Param("auctionOffer") AuctionOffer auctionOffer) throws Exception;

    List<AuctionOffer> getAuctionOffersForAuction(@Param("auction") Auction auction) throws Exception;

    void saveAuctionOffer(@Param("auctionOffer") AuctionOffer auctionOffer) throws Exception;

    void updateAuctionOffer(@Param("editAuctionOffer") AuctionOffer editAuctionOffer) throws Exception;

    void deleteAuctionOffer(@Param("auctionOffer") AuctionOffer auctionOffer) throws Exception;

    Integer countAuctionOffer(@Param("auctionOffer") AuctionOffer auctionOffer) throws Exception;

}
