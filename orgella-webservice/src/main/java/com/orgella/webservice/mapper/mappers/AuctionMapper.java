package com.orgella.webservice.mapper.mappers;

import com.orgella.webservice.model.Auction;
import com.orgella.webservice.model.Category;
import com.orgella.webservice.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AuctionMapper {

    Auction getAuctionById(@Param("auctionId") Integer auctionId) throws Exception;

    List<Auction> searchAuctions(@Param("name") String name, @Param("isBuyNow") Boolean isBuyNow, @Param("fromPrice") Float fromPrice, @Param("toPrice") Float toPrice) throws Exception;

    List<Auction> getAllAuctions() throws Exception;

    List<Auction> getAuctionsByCategory(@Param("category") Category category) throws Exception;

    List<Auction> getAuctionsByUser(@Param("user") User user) throws Exception;

    void saveAuction(@Param("auction") Auction auction) throws Exception;

    void updateAuction(@Param("editAuction") Auction editAuction) throws Exception;

    void deleteAuction(@Param("auction") Auction auction) throws Exception;

}
