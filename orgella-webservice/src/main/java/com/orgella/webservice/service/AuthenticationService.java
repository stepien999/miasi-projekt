package com.orgella.webservice.service;

import javax.mail.MessagingException;

import org.apache.commons.codec.digest.DigestUtils;

import com.orgella.webservice.dao.UserDao;
import com.orgella.webservice.mail.EmailSender;
import com.orgella.webservice.mapper.Injector;
import com.orgella.webservice.mapper.mappers.UserMapper;
import com.orgella.webservice.response.ServiceEnum;
import com.orgella.webservice.response.ServiceResponseTO;
import com.orgella.webservice.security.AESUtil;
import com.orgella.webservice.security.PasswordUtil;
import com.orgella.webservice.model.User;

public class AuthenticationService {

    public static boolean logIn(User userTO) throws Exception {
        UserDao userDao = new UserDao(Injector.getInstance().getMapper(UserMapper.class));
        User user = userDao.getUserByEmail(userTO.getEmail());
        //if (user != null && /*AESUtil.decrypt*/(userTO.getPassword()).equals(user.getPassword())) {
        if (user != null && DigestUtils.sha1Hex(userTO.getPassword()).equals(user.getPassword())) {
            return true;
        } else {
            return false;
        }
    }

    public static void logout() {
        Injector.getInstance().closeSession();
    }

    public static ServiceResponseTO signUp(User user) throws Exception {
        UserDao userDao = new UserDao(Injector.getInstance().getMapper(UserMapper.class));
        String pass = PasswordUtil.generatePassword();
        String hash = DigestUtils.sha1Hex(pass);
        user.setPassword(hash);
        try {
            if (userDao.saveUser(user)) {
                EmailSender.sendMail(user, pass);
                return new ServiceResponseTO(ServiceEnum.SUCCESS);
            } else {
                return new ServiceResponseTO(ServiceEnum.EXISTS);
            }
        } catch (MessagingException me) {
            userDao.deleteUser(user);
            return new ServiceResponseTO(ServiceEnum.MAIL_EXCEPTION, me);
        } catch (Exception e) {
            return new ServiceResponseTO(ServiceEnum.EXCEPTION, e);
        }
    }
}

