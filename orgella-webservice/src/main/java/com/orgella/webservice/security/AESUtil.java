package com.orgella.webservice.security;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AESUtil {

    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static final String IV = "I{/a3f_3gc!ag4{c";
    private static final String ENCRYPTION_KEY = "5<3>!3fc&s34,23s";
    private static final String ENCODING = "UTF-8";

    public static String decrypt(final String value) throws Exception {
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        final SecretKeySpec key = new SecretKeySpec(ENCRYPTION_KEY.getBytes(ENCODING), ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV.getBytes(ENCODING)));
        return new String(cipher.doFinal(new Base64().decode(value.getBytes(ENCODING))), ENCODING);
    }

    public static String encrypt(final String value) throws Exception {
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        final SecretKeySpec key = new SecretKeySpec(ENCRYPTION_KEY.getBytes(ENCODING), ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes(ENCODING)));
        return new Base64().encodeAsString(cipher.doFinal(value.getBytes(ENCODING)));
    }
}
