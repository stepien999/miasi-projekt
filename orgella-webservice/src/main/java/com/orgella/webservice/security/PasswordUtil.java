package com.orgella.webservice.security;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

public class PasswordUtil {

    public static String generatePassword() {
        Random random = new Random(10);
        return RandomStringUtils.randomAlphanumeric(random.nextInt(20));
    }
}

