INSERT INTO orgella.`user` (`user_id`,`adress`,`birth_date`,`city`,`country`,`email`,`first_name`,`last_name`,`password`,`phone_number`,`postal_code`) VALUES (1,'ul. Testowa 1','1992-10-04 01:00:00','Poznan','Polska','stepien999@gmail.com','Mateusz','Chudzinski','66168d3fc00a1d6a7843055f96e40c1fa0c91b90','456123456','60-156');
INSERT INTO orgella.`user` (`user_id`,`adress`,`birth_date`,`city`,`country`,`email`,`first_name`,`last_name`,`password`,`phone_number`,`postal_code`) VALUES (2,'dasds','1992-10-04 01:00:00','fsdf','fdsfs','chudzinski.mateusz@outlook.com','Mateudsz','Chudzinsdki','e4bd9cb81e83fa3b2bf4c2ef6f8ecadc25a07ace','456123ds456','fsfsfs');

INSERT INTO orgella.`category` (`category_id`,`category_description`,`category_name`) VALUES (1,'opis pierwszej kategorii','kategoria1');
INSERT INTO orgella.`category` (`category_id`,`category_description`,`category_name`) VALUES (2,'opis drugiej kategorii','kategoria2');

INSERT INTO orgella.`auction` (`auction_id`,`actual_stop_date`,`description`,`name`,`planned_stop_date`,`price`,`start_date`,`stop_reason`,`stopped`,`winner`,`is_buy_now`,`category_category_id`,`user_user_id`) VALUES (1,'2016-05-20 02:00:00','super aukcja','aukcja 1','2016-05-20 02:00:00',100,'2016-05-20 02:00:00',NULL,FALSE,NULL,TRUE,1,1);
INSERT INTO orgella.`auction` (`auction_id`,`actual_stop_date`,`description`,`name`,`planned_stop_date`,`price`,`start_date`,`stop_reason`,`stopped`,`winner`,`is_buy_now`,`category_category_id`,`user_user_id`) VALUES (2,'2016-05-25 02:00:00','super aukcja druga','aukcja 2','2016-05-27 02:00:00',600,'2016-05-25 02:00:00',NULL,FALSE,NULL,FALSE,1,1);
INSERT INTO orgella.`auction` (`auction_id`,`actual_stop_date`,`description`,`name`,`planned_stop_date`,`price`,`start_date`,`stop_reason`,`stopped`,`winner`,`is_buy_now`,`category_category_id`,`user_user_id`) VALUES (3,'2016-05-24 02:00:00','aukcja numer 3','aukcja3','2016-05-24 02:00:00',123,'2016-05-21 02:00:00','powód zatrzymania',TRUE,1,TRUE,2,1);

INSERT INTO orgella.`auction_offer` (`auction_offer_id`,`offer_date`,`price`,`auction_auction_id`,`user_user_id`) VALUES (1,'2016-05-26 02:00:00',350,1,1);
INSERT INTO orgella.`auction_offer` (`auction_offer_id`,`offer_date`,`price`,`auction_auction_id`,`user_user_id`) VALUES (2,'2016-05-26 02:00:00',400,1,2);

INSERT INTO orgella.`comments` (`comments_id`,`comment`,`possitive`,`fromUser_user_id`,`toUser_user_id`) VALUES (1,'od user1 do user 2',TRUE,1,2);
INSERT INTO orgella.`comments` (`comments_id`,`comment`,`possitive`,`fromUser_user_id`,`toUser_user_id`) VALUES (2,'od user2 do user1',FALSE,2,1);
